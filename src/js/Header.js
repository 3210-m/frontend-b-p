import React from 'react';
import src from '../assets/avatar.jpg';
class Header extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <header className="header">
        <img src={src} className="avatar image-size" />
        <h1 className="space">HELLO,</h1>
        <h2 className="space">
          MY NAME IS <span>{this.props.name}</span>
          <span>{this.props.age}</span>
          YO AND THIS IS MY RESUME/CV
        </h2>
      </header>
    );
  }
}
export default Header;
