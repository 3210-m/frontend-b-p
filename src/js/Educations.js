import React from 'react';

class Educations extends React.Component {
  constructor(props, context) {
    super(props, context);
  }
  render() {
    const educationlist = this.props.educations || [];
    // console.log(educationlist)
    return (
      <section>
        <h3>EDUCATION</h3>
        <ul id="education">
          {educationlist.map((item, index) => {
            return (
              <li key={index} className="li">
                <div className="year">{item.year}</div>
                <div className="container">
                  <span className="title space">{item.title}</span>
                  <p className="descrip space">{item.description}</p>
                </div>
              </li>
            );
          })}
        </ul>
      </section>
    );
  }
}
export default Educations;
