import education from './education';
export default class Person {
  constructor(result) {
    this._name = result.name;
    this._age = result.age;
    this._description = result.description;
    this._educations = result.educations.map(data => new education(data));
  }

  get name() {
    return this._name;
  }
  get age() {
    return this._age;
  }
  get description() {
    return this._description;
  }
  get educations() {
    return this._educations;
  }
}
