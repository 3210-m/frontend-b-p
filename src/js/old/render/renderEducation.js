import $ from 'jquery';

export default function renderEducation(result) {
  const { educations } = result;
  let html = '';
  educations.map(item => {
    html += `<li> <div class="year">${item.year}</div> <div class="container">
            <span class = "title space">${item.title}</span>
            <p class="descrip space">${item.description}</p></div> </li>`;
  });
  $('#education').html(html);
}
