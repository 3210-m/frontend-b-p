import $ from 'jquery';

export default function renderAboutMe(result) {
  $('#description').html(result.description);
}
