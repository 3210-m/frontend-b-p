import Person from '../Person';
import $ from 'jquery';

export default function renderHeader(result) {
  const person = new Person(result);
  $('#name').html(person.name);
  $('#age').html(person.age);
}
