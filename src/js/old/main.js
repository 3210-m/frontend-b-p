import renderHeader from './render/renderHeader';
import renderAboutMe from './render/renderAboutme';
import renderEducation from './render/renderEducation';
var url = 'http://localhost:3000/person';

fetch(url)
  .then(response => response.json())
  .then(result => {
    renderHeader(result);
    renderAboutMe(result);
    renderEducation(result);
  });
