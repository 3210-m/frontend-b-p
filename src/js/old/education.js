export default class education {
  constructor(data) {
    this._year = data.year;
    this._title = data.title;
    this._description = data.description;
  }
}
