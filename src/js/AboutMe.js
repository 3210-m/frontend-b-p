import React from 'react';

class AboutMe extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <section className="aboutMe">
        <p id="description">{this.props.description}</p>
      </section>
    );
  }
}

export default AboutMe;
