import './app.less';
// import './js/old/main.js';
import React from 'react';
import Header from './js/Header';
import AboutMe from './js/AboutMe';
import Educations from './js/Educations';

class App extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      person: {}
    };
  }
  componentDidMount() {
    fetch('http://localhost:3000/person')
      .then(response => response.json())
      .then(data => {
        this.setState({
          person: data
        });
      });
  }

  render() {
    const { name, age, description, educations } = this.state.person;
    console.log(educations, 'dh');
    return (
      <main>
        <Header name={name} age={age}></Header>
        <hr></hr>
        <AboutMe description={description}></AboutMe>
        <Educations educations={educations}></Educations>
      </main>
    );
  }
}
export default App;
