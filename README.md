# Requirement
![./assets/mockup.png](src/assets/mockup.png)
1. 在mockup上标示出语义标签
2. 在纸上做Tasking
3. 小步提交，至少3个提交
4. 按照效果图完成作业
5. (**扩展要求，可选！**)需要定义Person和Education两个对象
6. (**扩展要求，可选！**)通过eslint测试（命令参加Setup 4）
7. 可以Google，同时请注意你的时间


# Instruction

1. 样式参数定义可依据src/styles/variables.less
2. 头像地址assets/avatar.jpg
3. 实现异步请求数据
    
   启动Json Server（参加Setup 3），http://localhost:3000/person获取数据
4. jQuery命令

**设置元素的内容**

```javascript
$('#id').html("value");
```

**在元素结尾插入内容**

```javascript
const item = 'Item 1';
$('ul').append(`<li>${item}</li>`)
```

# Setup

1.安装包依赖

```
yarn install
```

2.启动服务 localhost:1234，更新文件会实时加载

```
yarn start
```

3.启动Json Server http://localhost:3000
```
yarn server
```

4.代码风格检测：

```
yarn lint
```
